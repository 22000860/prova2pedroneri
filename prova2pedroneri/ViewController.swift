//
//  ViewController.swift
//  prova2pedroneri
//
//  Created by COTEMIG on 20/01/44 AH.
//

import UIKit


struct Musica{
    let nomeMusica : String
    let nomeAlbum : String
    let nomeCantor : String
    let nomeImagemPequena: String
    let nomeImagemGrande: String
}
class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    
    var listaDeMusicas:[Musica] = []
    

    @IBOutlet weak var tableView : UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.listaDeMusicas.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"MinhaCelula",for : indexPath)as! MyCell
        let musica = self.listaDeMusicas[indexPath.row]
        
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.cantor.text = musica.nomeCantor
        cell.capa.image = UIImage(named: musica.nomeImagemPequena)
        
    return cell 
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row )
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalheViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusicas[indice]
        
        detalheViewController.nomeImagem = musica.nomeImagemGrande
        detalheViewController.nomeMusica = musica.nomeMusica
        detalheViewController.nomeAlbum = musica.nomeAlbum
        detalheViewController.nomeCantor = musica.nomeCantor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Pontos Cardeias ", nomeAlbum:"Album Vivo", nomeCantor: "Alceu Valenca", nomeImagemPequena: "capa_alceu_pequeno", nomeImagemGrande: "capa_alceu_grande"))
        self.listaDeMusicas.append(Musica(nomeMusica: "Menor Abandonado ", nomeAlbum:"Album Patota de Cosme ", nomeCantor: "Zeca pagodinho", nomeImagemPequena: "capa_zeca_pequeno", nomeImagemGrande: "capa_zeca_grande"))
        self.listaDeMusicas.append(Musica(nomeMusica: "Tiro ao alvaro ", nomeAlbum:"Album Adoniram Barbosa e Convidados", nomeCantor: "Adoniram Barbosa ", nomeImagemPequena: "capa_adoniran_pequeno", nomeImagemGrande: "capa_adoniran_grande"))
    }


}

